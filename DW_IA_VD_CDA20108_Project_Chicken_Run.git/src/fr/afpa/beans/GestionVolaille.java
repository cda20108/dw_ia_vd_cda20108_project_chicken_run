package fr.afpa.beans;

import java.util.Scanner;

public class GestionVolaille {

	private Volaille[] listeVolaille;
	private final static int NB_MAX_VOLAILLE = 7;
	private final static int NB_MAX_CARNARD = 4;
	private final static int NB_MAX_POULET = 5;
	private final static int NB_MAX_PAON = 3;
	private static int nbVolaille;

	// Valeur par defaut pour le prix au kilo et le poids d'abattage (car non
	// précisé dans l'énoncé !!! (ça fera un petit pain pour moi svp -> IA) )
	public final static int VALEUR_PAR_DEFAUT = 5;

	// Au cas ou on entre une liste toute faite
	public GestionVolaille(Volaille[] listeVolaille) {
		this.listeVolaille = listeVolaille;
	}

	public GestionVolaille() {
		this.listeVolaille = new Volaille[NB_MAX_VOLAILLE];

		PouletIMP.setPoidsAbattage(VALEUR_PAR_DEFAUT);
		CanardIMP.setPoidsAbattage(VALEUR_PAR_DEFAUT);

		PouletIMP.setPrixParKilo(VALEUR_PAR_DEFAUT);
		CanardIMP.setPrixParKilo(VALEUR_PAR_DEFAUT);

	}

	public Volaille[] getListeVolaille() {
		return listeVolaille;
	}

	public void setListeVolaille(Volaille[] listeVolaille) {
		this.listeVolaille = listeVolaille;
	}

	public final void vendreVolaille(Scanner in) {
		String choixVolaille = " - Quel type de volaille voulez-vous vendre ? \n";
		choixVolaille = choixVolaille + " (1) - Un poulet \n";
		choixVolaille = choixVolaille + " (2) - Un canard \n";
		choixVolaille = choixVolaille + " (3) - Retour \n";
		System.out.println(choixVolaille);
		System.out.print("Votre choix : ");

		int choix = in.nextInt();
		in.nextLine();

		int recherche = 0;
		boolean verif = false;

		while (choix != 3) {
			switch (choix) {
			case 1:

				// Verification qu'il y a au moins 1 poulet
				if (PouletIMP.getNbPoulet() > 0) {

					for (int i = 0; i < listeVolaille.length; i++) {

						// Verification que le poids du poulet est suffisant
						if (listeVolaille[i] != null && listeVolaille[i] instanceof PouletIMP
								&& ((PouletIMP) listeVolaille[i]).getPoids() >= PouletIMP.getPoidsAbattage()) {

							System.out.println("Le poulet numéro (" + listeVolaille[i].getId() + ") peut etre vendu");

							verif = true;
						}
					}

					// Si il y a au moins 1 poulet vendable
					if (verif) {
						recherche = rechercherVolaille(in, "Choisissez l'identifiant de poulet à vendre : ");
						if (recherche != -1) {
							((PouletIMP) listeVolaille[recherche]).volailleEnMoins();
							listeVolaille[recherche] = null;

							System.out.println("Le poulet a bien �t� vendu");
						}

					} else {
						System.out.println("Aucun poulet n'a de poids suffisant pour etre vendu");
					}

				} else {
					System.out.println("Il n'y a pas de poulet dans la ferme");
				}
				break;
			case 2:
				// Verification qu'il y a au moins 1 canard
				if (CanardIMP.getNbCanard() > 0) {

					for (int i = 0; i < listeVolaille.length; i++) {

						// Verification que le poids du canard est suffisant
						if (listeVolaille[i] != null && listeVolaille[i] instanceof CanardIMP
								&& ((CanardIMP) listeVolaille[i]).getPoids() >= CanardIMP.getPoidsAbattage()) {

							System.out.println("Le canard numéro (" + listeVolaille[i].getId() + ") peut etre vendu");
							verif = true;
						}
					}

					// Si il y a au moins 1 canard vendable
					if (verif) {
						recherche = rechercherVolaille(in, "Choisissez l'identifiant de poulet à vendre : ");
						if (recherche != -1) {
							((CanardIMP) listeVolaille[recherche]).volailleEnMoins();
							listeVolaille[recherche] = null;

							System.out.println("Le canard a bien �t� vendu");
						}

					} else {
						System.out.println("Aucun canard n'a de poids suffisant pour etre vendu");
					}

				} else {
					System.out.println("Il n'y a pas de canard dans la ferme");
				}
				break;
			case 3:
				System.out.println("Retour au menu...");
				break;
			default:
				System.out.println("Faites un bon choix !");
				break;
			}
			System.out.println(choixVolaille);
			System.out.print("Votre choix : ");
			choix = in.nextInt();
			in.nextLine();
		}

	}

	public final void voirTypeVolaille() {
		System.out.println("Nombre de POULET : " + PouletIMP.getNbPoulet());
		System.out.println("Nombre de CANARD: " + CanardIMP.getNbCanard());
		System.out.println("Nombre de PAON : " + Paon.getNbPaon());
	}

	public final void modifierPrix(Scanner in) {

		double prix = 0;

		String choixVolaille = " - Quel prix de volaille voulez-vous modifier ? \n";
		choixVolaille = choixVolaille + " (1) - Prix du poulet \n";
		choixVolaille = choixVolaille + " (2) - Prix du canard \n";
		choixVolaille = choixVolaille + " (3) - Retour \n";
		System.out.println(choixVolaille);
		System.out.print("Votre choix : ");

		int choix = in.nextInt();
		in.nextLine();

		while (choix != 3) {
			switch (choix) {
			case 1:
				System.out.print("Entrez le nouveau prix au kilo du poulet: ");
				prix = in.nextDouble();
				in.nextLine();

				PouletIMP.setPrixParKilo(prix);

				System.out.println("Le prix au kilo du poulet est de " + PouletIMP.getPrixParKilo() + " euros");
				break;
			case 2:
				System.out.print("Entrez le nouveau prix au kilo du Canard: ");
				prix = in.nextDouble();
				in.nextLine();

				CanardIMP.setPrixParKilo(prix);

				System.out.println("Le prix au kilo du canard est de " + CanardIMP.getPrixParKilo() + " euros");
				break;
			case 3:
				System.out.println("Retour au menu...");
				break;
			default:
				System.out.println("Faites un bon choix !");
				break;
			}
			System.out.println(choixVolaille);
			System.out.print("Votre choix : ");
			choix = in.nextInt();
			in.nextLine();
		}

	}

	public final int rechercherVolaille(Scanner in, String message) {

		// Message de l'element a rechercher (note IA : voir l'appel dans vendre
		// vollaille pour comprendre ceci)
		System.out.print(message);

		// id attribu� � une volaille d�s l'entr�e dans la ferme
		int idVol = in.nextInt();
		in.nextLine();
		
		System.out.println();

		for (int i = 0; i < listeVolaille.length; i++) {
			if (listeVolaille[i] != null && listeVolaille[i].getIdentifiant() == idVol) {
				return i;
			}
		}
		return -1;
	}

	// Prix Total des volaille Abattables

	public final void poidAbattage(Scanner in) {
		String choixVolaille = " - Pour quelle volaille voulez vous modifier le poids d'abattage ? \n";
		choixVolaille = choixVolaille + " (1) - Poids d'abattage du poulet \n";
		choixVolaille = choixVolaille + " (2) - Poids d'abattage du canard \n";
		choixVolaille = choixVolaille + " (3) - Retour \n";
		System.out.println(choixVolaille);
		System.out.print("Votre choix : ");

		int choix = in.nextInt();
		in.nextLine();
		System.out.println();

		double poidsAbattage = 0;

		while (choix != 3) {
			switch (choix) {
			case 1:
				System.out.print("Entrez le nouveau poids d'abattage des poulets : ");
				poidsAbattage = in.nextDouble();
				in.nextLine();

				PouletIMP.setPoidsAbattage(poidsAbattage);
				System.out.println("Le poids d'abattage des poulets à bien été modifié à : " + poidsAbattage);

				break;
			case 2:
				System.out.print("Entrez le nouveau poids d'abattage des canards : ");
				poidsAbattage = in.nextDouble();
				in.nextLine();

				CanardIMP.setPoidsAbattage(poidsAbattage);
				System.out.println("Le poids d'abattage des canards à bien été modifié à : " + poidsAbattage);
				break;
			default:
				System.out.println("Faites un bon choix !");
				break;
			}

			System.out.println(choixVolaille);
			System.out.print("Votre choix : ");
			choix = in.nextInt();
			in.nextLine();

		}

	}

	public final void prixTotalKebab() {
		double montant = 0;

		// Parcours de la liste des volailles

		for (int i = 0; i < listeVolaille.length; i++) {

			// si c'est un poulet assez gros, on multiple le poids avec le prix au kilo d'un
			// poulet

			if (listeVolaille[i] != null && listeVolaille[i] instanceof PouletIMP
					&& ((PouletIMP) listeVolaille[i]).getPoids() >= PouletIMP.getPoidsAbattage()) {
				montant += (PouletIMP.getPrixParKilo() * ((PouletIMP) listeVolaille[i]).getPoids());
			}
			// si c'est un canard assez gros, on multiple le poids avec le prix au kilo d'un
			// canard

			else if (listeVolaille[i] != null && listeVolaille[i] instanceof CanardIMP
					&& ((CanardIMP) listeVolaille[i]).getPoids() >= CanardIMP.getPoidsAbattage()) {

				montant += (CanardIMP.getPrixParKilo() * ((CanardIMP) listeVolaille[i]).getPoids());
			}
			// si c'est Paon ou un enclos vide, nous passons à l'enclos suivant
		}
		System.out.println("Le prix total des Volailles Abatables est de : " + montant + " euros");
	}

	// modifier le poids d'une volaille (poulet ou canard)
	public final void modifierPoids(Scanner in) {

		double pesee = 0;

		int positionV = rechercherVolaille(in, "Entrez l'identifiant de la volaille Ã  modifier : ");

		if (positionV != -1) {

			System.out.print("Quel est le nouveau poids de la volaille ? : ");
			pesee = in.nextDouble();
			in.nextLine();

			if (listeVolaille[positionV] instanceof CanardIMP) {
				((CanardIMP) listeVolaille[positionV]).setPoids(pesee);
			} else if (listeVolaille[positionV] instanceof PouletIMP) {
				((PouletIMP) listeVolaille[positionV]).setPoids(pesee);
			} else {

				System.out.println("On ne mange pas les Paons !!!");
			}
		} else {
			System.out.println("Volaille Introuvable");
		}
	}

	public final void ajoutVolaille(Scanner in) {

		if (nbVolaille == NB_MAX_VOLAILLE) {
			System.out.println("Nombre de Volaillle maximal atteint");

		} else {
			String choixVolaille = " Quelle volaille voulez vous ajouter ? \n";
			choixVolaille = choixVolaille + " 1 - Ajouter un poulet \n";
			choixVolaille = choixVolaille + " 2 - Ajouter un canard \n";
			choixVolaille = choixVolaille + " 3 - Ajouter un Paon \n";
			choixVolaille = choixVolaille + " 4 - Retour \n";
			System.out.println(choixVolaille);
			System.out.print("Votre choix : ");

			int choix = in.nextInt();
			in.nextLine();

			while (choix != 4) {
				switch (choix) {

				case 1:

					if (PouletIMP.getNbPoulet() >= NB_MAX_POULET) {
						System.out.println("Maximum poulet atteint");
					} else {

						nbVolaille++;

						for (int i = 0; i < listeVolaille.length; i++) {

							if (listeVolaille[i] == null) {

								listeVolaille[i] = new PouletIMP();
								((PouletIMP) listeVolaille[i]).volailleEnPlus();
								System.out.println(
										"Poulet ajout� et son id est : " + ((PouletIMP) listeVolaille[i]).getId());

								break;
							}
						}

					}

					break;
				case 2:

					if (CanardIMP.getNbCanard() >= NB_MAX_CARNARD) {
						System.out.println("Maximum canard atteint");
					} else {

						nbVolaille++;
						for (int i = 0; i < listeVolaille.length; i++) {

							if (listeVolaille[i] == null) {

								listeVolaille[i] = new CanardIMP();
								((CanardIMP) listeVolaille[i]).volailleEnPlus();
								System.out.println(
										"Canard ajout� et son id est : " + ((CanardIMP) listeVolaille[i]).getId());

								break;
							}
						}

					}
					break;
				case 3:

					if (Paon.getNbPaon() >= NB_MAX_PAON) {
						System.out.println("Maximum paon atteint");

					} else {

						nbVolaille++;

						for (int i = 0; i < listeVolaille.length; i++) {

							if (listeVolaille[i] == null) {

								listeVolaille[i] = new Paon();
								((Paon) listeVolaille[i]).volailleEnPlus();
								System.out.println("Paon ajout� et son id est : " + ((Paon) listeVolaille[i]).getId());

								break;
							}
						}

					}
					break;

				case 4:

					System.out.println("Au revoir");

					break;

				default:

					System.out.println("Reiterez votre choix");

					break;
				}
				System.out.println(choixVolaille);
				System.out.print("Votre choix : ");

				choix = in.nextInt();
				in.nextLine();
			}
		}
	}

	public final void afficherMenuPoidsAbattage(Scanner in) {

		String menu = "------------------------------- MENU ABATTAGE ---------------------------------------------- \n";
		menu = menu
				+ "*                                                                                              * \n";
		menu = menu
				+ "*                              1- Modifier poids Abattage Canard                               * \n";
		menu = menu
				+ "*                              2- Modifier poids Abattage Poulet                               * \n";
		menu = menu
				+ "*                              3- Retourner au menu Exploitation                               * \n";
		menu = menu
				+ "------------------------------------------------------------------------------------------------ \n";

		int choix = 0;

		System.out.println(menu);
		System.out.print("Veuillez faire votre choix:");
		choix = in.nextInt();
		in.nextLine();
		System.out.println();

		if (choix == 1 || choix == 2) {

			switch (choix) {
			case 1:
				CanardIMP c = new CanardIMP();
				c.modifierPoidsAbattage(in);
				break;

			case 2:
				PouletIMP p = new PouletIMP();
				p.modifierPoidsAbattage(in);
				break;

			}
		}
	}

	public final void rendrePaon(Scanner in) {
		
		int indicepaon = rechercherVolaille(in, "Veuillez renseigner l'identifiant du Paon");

		if (indicepaon != -1) {
			if (listeVolaille[indicepaon] instanceof Paon) {
				((Paon) listeVolaille[indicepaon]).volailleEnMoins();
				listeVolaille[indicepaon] = null;
				System.out.println("Le Paon a bien �t� rendu");
			} else {
				System.out.println("Ce n'est pas un paon");
			}
		} else {
			System.out.println("Volaille introuvable");
		}
	}

	public final void afficherMenuExploitation(Scanner in) {

		String menu = "------------------------------- MENU EXPLOITATION CDA  ----------------------------------------- \n";
		menu = menu
				+ "*                                                                                                  * \n";
		menu = menu
				+ "*                              1- Ajouter une volaille                                             * \n";
		menu = menu
				+ "*                              2- Modifier le poids pour l'abattage                                * \n";
		menu = menu
				+ "*                              3- Modifier le prix au kilo du jour                                 * \n";
		menu = menu
				+ "*                              4- Modifier le poids d'une volaille                                 * \n";
		menu = menu
				+ "*                              5- Voir le nombre de volailles par type                             * \n";
		menu = menu
				+ "*                              6- Voir le prix total des volailles abattables                      * \n";
		menu = menu
				+ "*                              7- Vendre une volaille                                              * \n";
		menu = menu
				+ "*                              8- Rendre un paon au parc                                     * \n";
		menu = menu
				+ "*                              9- Quitter                                                          * \n";
		menu = menu
				+ "*                                                                                                  * \n";
		menu = menu
				+ "---------------------------------------------------------------------------------------------------- \n";

		int choix = 0;

		while (choix != 9) {

			System.out.println(menu);
			System.out.println("Veuillez faire votre choix:");
			choix = in.nextInt();
			in.nextLine();
			switch (choix) {

			case 1:
				// AjouterUneVolaille();
				ajoutVolaille(in);
				break;

			case 2:
				// Modifier le poids pour l'abattage
				afficherMenuPoidsAbattage(in);
				break;

			case 3:
				// Modifier le prix au kilo du jour
				modifierPrix(in);
				break;

			case 4:
				// Modifier le poids d'une volaille
				modifierPoids(in);
				break;

			case 5:
				// Voir le nombre de volailles par type
				voirTypeVolaille();
				break;

			case 6:
				// Voir le prix total des volailles abattables
				prixTotalKebab();
				break;

			case 7:
				// Vendre une volaille
				vendreVolaille(in);
				break;

			case 8:
				// Rendre un paon au parc.
				rendrePaon(in);
				break;
			case 9:
				// Quitter
				System.out.println("Au revoir !");
				break;
			}

		}
	}

}
