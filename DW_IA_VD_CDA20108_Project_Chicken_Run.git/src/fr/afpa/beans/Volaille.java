package fr.afpa.beans;


public abstract class Volaille {

	private static int id;
	private int identifiant;
	
	public Volaille() {
		super();
		this.identifiant = ++id;
	}
	

	public int getId() {
		return id;
	}

	
	public int getIdentifiant() {
		return identifiant;
	}
	
	public void setIdentifiant(int identifiant) {
		this.identifiant = identifiant;
	}
	
	public abstract void volailleEnMoins();
	
	public abstract void volailleEnPlus();

	
	
	
}
