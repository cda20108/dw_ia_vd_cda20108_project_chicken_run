package fr.afpa.beans;

import java.util.Scanner;

public final class CanardIMP extends Kebab implements IChickenRip {

	private static int nbCanard;
	private static double prixParKilo;
	private static double poidsAbattage;
	
	public CanardIMP() {
		
	}
	
	public CanardIMP(double poids, int ageEnSemaine) {
		super(poids, ageEnSemaine);
		CanardIMP.nbCanard++;
	}

	public static int getNbCanard() {
		return nbCanard;
	}

	public static void setNbCanard(int nbCanard) {
		CanardIMP.nbCanard = nbCanard;
	}



	@Override
	public void volailleEnMoins() {
		CanardIMP.nbCanard--;
	}

	public static double getPrixParKilo() {
		return prixParKilo;
	}

	public static void setPrixParKilo(double prixParKilo) {
		CanardIMP.prixParKilo = prixParKilo;
	}

	public static double getPoidsAbattage() {
		return poidsAbattage;
	}

	public static void setPoidsAbattage(double poidsAbattage) {
		CanardIMP.poidsAbattage = poidsAbattage;
	}

	@Override
	public final void modifierPoidsAbattage(Scanner in) {
		System.out.print("Entrez le nouveau poids d'abattage du Canard en Kg: ");
		poidsAbattage = in.nextDouble();
		in.nextLine();

		System.out.println("Le nouveau poids d'abattage du canard est de " + poidsAbattage + " Kg");
		
	}

	@Override
	public void volailleEnPlus() {
		CanardIMP.nbCanard++;
		
	}

}