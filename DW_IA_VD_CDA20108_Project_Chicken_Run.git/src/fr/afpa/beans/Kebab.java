package fr.afpa.beans;


public abstract class Kebab extends Volaille {

	private double poids;
	private int ageEnSemaine;

	public Kebab(double poids, int ageEnSemaine) {
		this.poids = poids;
		this.ageEnSemaine = ageEnSemaine;
	}

	public Kebab() {
		poids = GestionVolaille.VALEUR_PAR_DEFAUT;
		ageEnSemaine = 3;
	}

	public double getPoids() {
		return poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public int getAgeEnSemaine() {
		return ageEnSemaine;
	}

	public void setAgeEnSemaine(int ageEnSemaine) {
		this.ageEnSemaine = ageEnSemaine;
	}

	
	

}
