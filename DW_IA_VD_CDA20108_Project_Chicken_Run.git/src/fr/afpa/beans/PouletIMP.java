package fr.afpa.beans;

import java.util.Scanner;

public final class PouletIMP extends Kebab implements IChickenRip {
	
	private static int nbPoulet;
	private static double prixParKilo;
	private static double poidsAbattage;
	
	public PouletIMP() {
		
	}

	public PouletIMP(double poids, int ageEnSemaine) {
		super(poids, ageEnSemaine);
		PouletIMP.nbPoulet++;
	}

	

	public static double getPrixParKilo() {
		return prixParKilo;
	}

	public static void setPrixParKilo(double prixParKilo) {
		PouletIMP.prixParKilo = prixParKilo;
	}


	
	
	public static int getNbPoulet() {
		return nbPoulet;
	}

	public static void setNbPoulet(int nbPoulet) {
		PouletIMP.nbPoulet = nbPoulet;
	}

	@Override
	public void volailleEnMoins() {
		PouletIMP.nbPoulet--;
	}

	@Override
	public void volailleEnPlus() {
		PouletIMP.nbPoulet++;
		
	}

	public static double getPoidsAbattage() {
		return poidsAbattage;
	}

	public static void setPoidsAbattage(double poidsAbattage) {
		PouletIMP.poidsAbattage = poidsAbattage;
	}



	@Override
	public final void modifierPoidsAbattage(Scanner in) {
		System.out.print("Entrez le nouveau poids d'abattage du poulet en Kg : ");
		poidsAbattage = in.nextDouble();
		in.nextLine();
		
		System.out.println("Le nouveau poids d'abattage du poulet est de " + poidsAbattage + " Kg");
		
	}
}
