package fr.afpa.beans;

public final class Paon extends Volaille {
	

	private static int nbPaon;

	public Paon() {
		super();	
	}

	
	public static int getNbPaon() {
		return nbPaon;
	}
	
	public static void setNbPaon(int nbPaon) {
		Paon.nbPaon = nbPaon;
	}
	
	@Override
	public void volailleEnMoins() {
		Paon.nbPaon--;
	}


	@Override
	public void volailleEnPlus() {
		Paon.nbPaon++;
		
	}

}
